/**
 * Setup audiofiles: follow folder/filename-structure from audio-folder
 * @type {string[]}
 */
const head = document.querySelector('.patrick-head');
const files = {
    uitspraken: [
        ['aannemer.mp3', 'Aannemer'],
        ['depreciated.mp3', 'Deprecated'],
        ['hey-jew.mp3', 'Hey Jude'],
        ['problemen.mp3', 'Problemen'],
        ['anatomic-design.mp3', 'Atomic Design'],
        ['steakoverflow.mp3', 'Stack Overflow'],
        ['anti-analysing.mp3', 'Anti-Aliasing'],
        ['niracecura.mp3', 'Nicaragua'],
    ],
    geweld: [
        ['patrick-ik-wil-zo-graag-met-je-overleggen.mp3', 'overleggen'],
        ['patrick-stop-hiermee.mp3', 'stop hiermee'],
        ['patrick-stoppen-krakkrak.mp3', 'stoppen'],
        ['patrick-we-kunnen-toch-gewoon-een-gesprek-WEG.mp3', 'gewoon gesprek'],
    ],
    podcast: [
        ['iedereen-die-ik-spreek-vertelt-me-hoe-aantrekkelijk-hij-was.mp3', 'aantrekkelijk'],
        ['ja-dat-was-patrick.mp3', 'dat was patrick'],
        ['pa-patrick-is-dood.mp3', 'patrick is dood'],
        ['patrick-was-iemand-lang-leve-de-lol.mp3', 'lang leve de lol'],
    ],
    memes: [
        ['5-euros-op-je-muil-gauw.mp3', '5 euros?'],
        ['blikje-in-de-water.mp3', 'blikje'],
        ['Charlene-CTRL-ALT-DEL-Toetsencombinatie.mp3', 'ctrl-alt-del'],
        ['Fietsopa-What-No-money-Here-suck-a-cock.mp3', 'no money?'],
        ['je-bent-geen-fabriek.mp3', 'geen fabriek'],
        ['klaas-faak-jij-komen-hier-praten-tegen-mij.mp3', 'jij komen hier?'],
        ['KRAKAKA.mp3', 'krakaka'],
    ],
    maartenvanrossem: [
        ['proempriem.mp3', 'Proem priem'],
    ],
}

/**
 * Create buttons
 */
const root = document.querySelector('.buttons');
for (const folder in files) {
    // render title
    h2 = document.createElement('h2');
    h2.textContent = folder;
    root.insertAdjacentElement('beforeend', h2);

    // render button
    files[folder].forEach(function (file) {
        button = document.createElement('button');
        button.classList.add('btn--' + folder);
        button.textContent = file[1];
        button.addEventListener('click', function () {
            playAudio('./audio/' + folder + '/' + file[0]);
        });
        document.querySelector('.buttons').insertAdjacentElement('beforeend', button);
    });
}

/**
 * Create audioplayer with event listeners
 * @type {HTMLElement}
 */
const audio = document.createElement('audio');
audio.setAttribute('id', 'sound');
audio.addEventListener('loadedmetadata', function () {
    console.log(this.duration);
});
audio.addEventListener('play', function () {
    startTalking();
});
audio.addEventListener('ended', function () {
    stopTalking();
});
document.body.insertAdjacentElement('beforeend', audio);

/**
 * Play audio
 * @param file
 */
function playAudio(file) {
    audio.setAttribute('src', file);
    audio.play();
}

/**
 * Make Head talk
 */
function startTalking() {
    head.classList.add('patrick-head--talking');
}

/**
 * Stop Head talk
 */
function stopTalking() {
    head.classList.remove('patrick-head--talking');
}
