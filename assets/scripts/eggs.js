const egg = new Egg();

// toggle spuit 11
egg.addCode("s,p,u,i,t,1,1", function() {
    const body = document.querySelector('.patrick-body');
    body.classList.toggle('patrick-body--spuit11');
}).listen();

// toggle fullscreen
egg.addCode("f,f", function() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
}).listen();
